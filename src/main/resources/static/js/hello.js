angular.module('hello', []).controller('home', function($http, $scope) {
	var ctrl = this;

	$scope.sendRequest = function() {
		$http.post('/validate', ctrl.data).then(successCallback, errorCallback);
	};

	$scope.autoExpand = function(e) {
		var element = typeof e === 'object' ? e.target : document.getElementById(e);
		var scrollHeight = element.scrollHeight -10;
		element.style.height =  scrollHeight + "px";
	};

	function successCallback(response) {
		ctrl.data = JSON.stringify(response.data.result);
	}

	function errorCallback(response) {
		alert(response.data);
	}
});