package com.example.demo.rest;

import com.example.demo.dto.TradeInformation;
import com.example.demo.dto.ValidationRequest;
import com.example.demo.dto.ValidationResponse;
import com.example.demo.service.ValidationService;

import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ValidationApi {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ValidationService validationService;

	@RequestMapping(path = "/validate",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE,
			method = RequestMethod.POST)
	public ValidationResponse validate(@RequestBody TradeInformation[] tradeInformations) {
		Thread.currentThread().setName(UUID.randomUUID().toString());
		logger.info("Processing: {}", Arrays.toString(tradeInformations));
		return validationService.validate(ValidationRequest.builder().tradeInformations(Arrays.asList(tradeInformations)).build());
	}

	/*@RequestMapping(params = "/validate",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE,
			method = RequestMethod.POST)
	public ValidationResponse validate(@RequestBody TradeInformation tradeInformation) {
		Thread.currentThread().setName(UUID.randomUUID().toString());
		logger.info("Processing: {}", tradeInformation);
		return validationService.validate(ValidationRequest.builder().tradeInformations(Collections.singletonList(tradeInformation)).build());
	}
*/
}
