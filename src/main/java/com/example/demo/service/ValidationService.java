package com.example.demo.service;

import com.example.demo.dto.TradeInformation;
import com.example.demo.dto.TradeType;
import com.example.demo.dto.ValidationRequest;
import com.example.demo.dto.ValidationResponse;
import com.example.demo.service.validation.AbstractValidationHandler;
import com.example.demo.service.validation.util.ResponseHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidationService {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Map<TradeType, AbstractValidationHandler> handlerMap;

	@Autowired
	private ResponseHelper responseHelper;

	@Autowired
	public ValidationService(Collection<AbstractValidationHandler> handlerMap) {
		this.handlerMap = new ConcurrentHashMap<>();
		handlerMap.forEach(it -> this.handlerMap.put(it.getType(), it));
	}

	public ValidationResponse validate(ValidationRequest data) {
		ValidationResponse response = new ValidationResponse();
		for (TradeInformation tradeInformation : data.getTradeInformations()) {
			AbstractValidationHandler handler = handlerMap.get(tradeInformation.getType());
			if (handler == null) {
				String key = responseHelper.getTradeInformationKey(tradeInformation);
				response.merge(new ValidationResponse().appendMessage(key, "No handler"));
			} else {
				logger.info("Processing by handler: {}", handler.getClass().getSimpleName());
				response.merge(handler.validate(tradeInformation));
			}
		}
		return response;
	}

}
