package com.example.demo.service.validation;

import com.example.demo.dto.TradeInformation;
import com.example.demo.dto.TradeType;
import com.example.demo.dto.ValidationResponse;
import com.example.demo.fixer.FixerClient;
import com.example.demo.fixer.FixerResponse;
import com.example.demo.service.validation.util.CurrencyHelper;
import com.example.demo.service.validation.util.ResponseHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractValidationHandler {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private static final Collection<String> supportedCounterparties = Arrays.asList("PLUTO1", "PLUTO2");

	@Autowired
	private CurrencyHelper currencyHelper;
	@Autowired
	private FixerClient fixerClient;
	@Autowired
	private ResponseHelper responseHelper;

	public abstract TradeType getType();

	public abstract ValidationResponse validate(TradeInformation tradeInformation);

	protected ValidationResponse performBaseValidation(TradeInformation tradeInformation) {
		ValidationResponse validationResponse = new ValidationResponse();
		if (tradeInformation.getCustomer() == null) {
			responseHelper.logAndAppend(validationResponse, "Customer information is required", tradeInformation, getClass());
		} else {
			if (!supportedCounterparties.contains(tradeInformation.getCustomer())) {
				responseHelper.logAndAppend(validationResponse, "Customer is not supported", tradeInformation, getClass());
			}
		}
		if (!currencyHelper.isPairValid(tradeInformation.getCcyPair())) {
			responseHelper.logAndAppend(validationResponse, "Currency pair is not valid", tradeInformation, getClass());
		}
		if (tradeInformation.getValueDate() == null) {
			responseHelper.logAndAppend(validationResponse, "ValueDate is required", tradeInformation, getClass());
		} else if (tradeInformation.getTradeDate() == null) {
			responseHelper.logAndAppend(validationResponse, "TradeDate is required", tradeInformation, getClass());
		} else {
			if (tradeInformation.getValueDate().before(tradeInformation.getTradeDate())) {
				responseHelper.logAndAppend(validationResponse, "ValueDate cannot be before trade date", tradeInformation, getClass());
			} else {
				FixerResponse fixerResponse = fixerClient.getForCurrencyAndDate(tradeInformation.getValueDate());
				if (fixerResponse == null) {
					responseHelper.logAndAppend(validationResponse, "Could not validate valueDate against weekend or non-working day - try again later", tradeInformation, getClass());
				} else {
					if (!fixerResponse.getRates().containsKey(tradeInformation.getPremiumCcy())) {
						responseHelper.logAndAppend(validationResponse, "Value date not valid", tradeInformation, getClass());
					}
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					if (!df.format(tradeInformation.getValueDate()).equals(df.format(fixerResponse.getDate()))) {
						responseHelper.logAndAppend(validationResponse, "Value date is not correct", tradeInformation, getClass());
					}
				}
			}
		}
		return validationResponse;
	}


}
