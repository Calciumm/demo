package com.example.demo.service.validation.util;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.stereotype.Component;

@Component
public class CurrencyHelper {
	private static final Collection<String> allCurrencies;

	static {
		allCurrencies = Arrays.asList(
				"AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD", "AWG", "AZN",
				"BAM", "BBD", "BDT", "BGN", "BHD", "BIF", "BMD", "BND", "BOB", "BOV", "BRL", "BSD", "BTN", "BWP", "BYN", "BZD",
				"CAD", "CDF", "CHE", "CHF", "CHW", "CLF", "CLP", "CNY", "COP", "COU", "CRC", "CUC", "CUP", "CVE", "CZK",
				"DJF", "DKK", "DOP", "DZD",
				"EGP", "ERN", "ETB", "EUR",
				"FJD", "FKP",
				"GBP", "GEL", "GHS", "GIP", "GMD", "GNF", "GTQ", "GYD",
				"HKD", "HNL", "HRK", "HTG", "HUF",
				"IDR", "ILS", "INR", "IQD", "IRR", "ISK",
				"JMD", "JOD", "JPY",
				"KES", "KGS", "KHR", "KMF", "KPW", "KRW", "KWD", "KYD", "KZT",
				"LAK", "LBP", "LKR", "LRD", "LSL", "LYD",
				"MAD", "MDL", "MGA", "MKD", "MMK", "MNT", "MOP", "MRO", "MUR", "MVR", "MWK", "MXN", "MXV", "MYR", "MZN",
				"NAD", "NGN", "NIO", "NOK", "NPR", "NZD",
				"OMR",
				"PAB", "PEN", "PGK", "PHP", "PKR", "PLN", "PYG",
				"QAR",
				"RON", "RSD", "RUB", "RWF",
				"SAR", "SBD", "SCR", "SDG", "SEK", "SGD", "SHP", "SLL", "SOS", "SRD", "SSP", "STD", "SVC", "SYP", "SZL",
				"THB", "TJS", "TMT", "TND", "TOP", "TRY", "TTD", "TWD", "TZS",
				"UAH", "UGX", "USD", "USN", "UYI", "UYU", "UZS",
				"VEF", "VND", "VUV",
				"WST",
				"XAF", "XAG", "XAU", "XBA", "XBB", "XBC", "XBD", "XCD", "XDR", "XOF", "XPD", "XPF", "XPT", "XSU", "XTS", "XUA", "XXX",
				"YER",
				"ZAR", "ZMW", "ZWL"
		);
	}

	public boolean isValid(String currency) {
		return allCurrencies.contains(currency);
	}

	public boolean isPairValid(String currencyPair) {
		return allCurrencies.contains(currencyPair.substring(0, 3))
				&& allCurrencies.contains(currencyPair.substring(3, currencyPair.length()));
	}
}
