package com.example.demo.service.validation;

import com.example.demo.dto.TradeInformation;
import com.example.demo.dto.TradeType;
import com.example.demo.dto.ValidationResponse;

import org.springframework.stereotype.Component;

@Component
public class SpotValidationHandler extends AbstractValidationHandler {

	@Override
	public TradeType getType() {
		return TradeType.SPOT;
	}

	@Override
	public ValidationResponse validate(TradeInformation tradeInformation) {
		logger.info("Validation in progress: {}", tradeInformation);
		ValidationResponse result = performBaseValidation(tradeInformation);
		//TODO: I don't know how to validate valueDate against productType, should I use some external API?
		return result;
	}
}
