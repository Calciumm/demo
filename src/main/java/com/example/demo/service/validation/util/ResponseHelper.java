package com.example.demo.service.validation.util;

import com.example.demo.dto.TradeInformation;
import com.example.demo.dto.ValidationResponse;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ResponseHelper {
	private Map<Class, Logger> loggers = new HashMap<>();


	public void logWarn(TradeInformation tradeInformation, String s, Class loggerClass) {
		if (!loggers.containsKey(loggerClass)) {
			loggers.put(loggerClass, LoggerFactory.getLogger(loggerClass));
		}
		loggers.get(loggerClass).warn(s + " in: {}", tradeInformation);
	}

	public String getTradeInformationKey(TradeInformation tradeInformation) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return tradeInformation.getCustomer() + '_' + df.format(tradeInformation.getTradeDate()) + '_' + tradeInformation.getTrader();
	}

	public void logAndAppend(final ValidationResponse validationResponse, String msg, TradeInformation tradeInformation, Class loggerClass) {
		logWarn(tradeInformation, msg, loggerClass);
		validationResponse.appendMessage(getTradeInformationKey(tradeInformation), msg);
	}
}
