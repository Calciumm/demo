package com.example.demo.service.validation;

import com.example.demo.dto.TradeInformation;
import com.example.demo.dto.TradeType;
import com.example.demo.dto.ValidationResponse;

import org.springframework.stereotype.Component;

@Component
public class ForwardValidationHandler extends AbstractValidationHandler {

	@Override
	public TradeType getType() {
		return TradeType.FORWARD;
	}

	@Override
	public ValidationResponse validate(TradeInformation tradeInformation) {
		logger.info("Validation in progress: {}", tradeInformation);
		ValidationResponse result = performBaseValidation(tradeInformation);
		//TODO: Same as in Spot Validator, cannot validate value date against product type without consultation.
		return result;
	}
}
