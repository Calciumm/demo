package com.example.demo.service.validation;

import com.example.demo.dto.TradeInformation;
import com.example.demo.dto.TradeStyle;
import com.example.demo.dto.TradeType;
import com.example.demo.dto.ValidationResponse;
import com.example.demo.service.validation.util.ResponseHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OptionValidationHandler extends AbstractValidationHandler {

	@Autowired
	private ResponseHelper responseHelper;

	@Override
	public TradeType getType() {
		return TradeType.OPTION;
	}

	@Override
	public ValidationResponse validate(TradeInformation tradeInformation) {
		logger.info("Validation in progress: {}", tradeInformation);
		ValidationResponse result = performBaseValidation(tradeInformation);
		if (tradeInformation.getStyle() == null) {
			responseHelper.logAndAppend(result, "Style is required", tradeInformation, getClass());
		} else {
			if (TradeStyle.NOT_SUPPORTED.equals(tradeInformation.getStyle())) {
				responseHelper.logAndAppend(result, "Style is not supported", tradeInformation, getClass());
			} else {
				if (TradeStyle.AMERICAN.equals(tradeInformation.getStyle())) {
					if (tradeInformation.getExcerciseStartDate() == null) {
						responseHelper.logAndAppend(result, "Excercise start date is required for American style", tradeInformation, getClass());
					}
					if (tradeInformation.getExcerciseStartDate().before(tradeInformation.getTradeDate())
							|| tradeInformation.getExcerciseStartDate().equals(tradeInformation.getTradeDate())
							|| tradeInformation.getExcerciseStartDate().equals(tradeInformation.getExpiryDate())
							|| tradeInformation.getExcerciseStartDate().after(tradeInformation.getExpiryDate())) {
						responseHelper.logAndAppend(result, "Excercise start date should be after trade date but before expiry date", tradeInformation, getClass());
					}
				}
			}
		}
		if (tradeInformation.getDeliveryDate() == null) {
			responseHelper.logAndAppend(result, "Delivery date is required", tradeInformation, getClass());
		} else if (tradeInformation.getExpiryDate() == null) {
			responseHelper.logAndAppend(result, "Expiry date is required", tradeInformation, getClass());
		} else {
			if (tradeInformation.getExpiryDate().after(tradeInformation.getDeliveryDate())
					|| tradeInformation.getExpiryDate().equals(tradeInformation.getDeliveryDate())) {
				responseHelper.logAndAppend(result, "Expiry date should be before delivery date", tradeInformation, getClass());
			}
		}
		if (tradeInformation.getPremiumDate() == null) {
			responseHelper.logAndAppend(result, "Premium date is required", tradeInformation, getClass());
		} else if (tradeInformation.getPremiumDate().after(tradeInformation.getDeliveryDate())
				|| tradeInformation.getPremiumDate().equals(tradeInformation.getDeliveryDate())) {
			responseHelper.logAndAppend(result, "Premium date should be before delivery date", tradeInformation, getClass());
		}

		if (result.isValid()) {
			return new ValidationResponse().appendMessage(responseHelper.getTradeInformationKey(tradeInformation), "OK");
		} else {
			return result;
		}
	}
}
