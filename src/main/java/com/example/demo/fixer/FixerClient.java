package com.example.demo.fixer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class FixerClient {
	private static final String FIXER_ENDPOINT = "http://api.fixer.io";

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private final RestTemplate restTemplate = new RestTemplate();

	public FixerResponse getForCurrencyAndDate(Date valueDate) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return restTemplate.getForObject(FIXER_ENDPOINT + "/" + df.format(valueDate), FixerResponse.class);
	}
}
