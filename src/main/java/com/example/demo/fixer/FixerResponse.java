package com.example.demo.fixer;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Tolerate;

@Setter
@Getter
@Builder
@ToString
public class FixerResponse {
	private String base;
	private Date date;
	private Map<String, BigDecimal> rates;

	@Tolerate
	public FixerResponse() {
	}
}
