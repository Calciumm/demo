package com.example.demo.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;

@Setter
@Getter
@Builder
public class ValidationResponse implements Serializable {
	private static final long serialVersionUID = -410564814981612778L;

	private Map<String, String> result;

	@Tolerate
	public ValidationResponse() {
	}

	@Tolerate
	public ValidationResponse appendMessage(String key, String message) {
		if (result == null) {
			result = new HashMap<>();
		}
		if (result.containsKey(key)) {
			result.merge(key, message, (s, s2) -> s + ". " + s2);
		} else {
			result.put(key, message);
		}
		return this;
	}

	@Tolerate
	public ValidationResponse merge(ValidationResponse response) {
		if (response != null && response.getResult() != null && !response.getResult().isEmpty()) {
			response.getResult().forEach(this::appendMessage);
		}
		return this;
	}

	@Tolerate
	public boolean isValid() {
		return result == null || result.isEmpty();
	}
}
