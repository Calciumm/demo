package com.example.demo.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Tolerate;

@Getter
@Setter
@Builder
@ToString
public class TradeInformation implements Serializable {
	private static final long serialVersionUID = -3720247498626917761L;

	private String customer;
	private String ccyPair;
	private TradeType type;
	private TradeDirection direction;
	private String strategy;
	private Date tradeDate;
	private BigDecimal amount1;
	private BigDecimal amount2;
	private TradeStyle style;
	private BigDecimal rate;
	private Date deliveryDate;
	private Date expiryDate;
	private Date excerciseStartDate;
	private String payCcy;
	private BigDecimal premium;
	private String premiumCcy;
	private String premiumType;
	private Date premiumDate;
	private Date valueDate;
	private String legalEntity;
	private String trader;

	@Tolerate
	public TradeInformation() {
	}
}
