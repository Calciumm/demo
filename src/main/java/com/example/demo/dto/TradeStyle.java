package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.stream.Stream;

public enum TradeStyle {
	EUROPEAN, AMERICAN, NOT_SUPPORTED;

	@JsonCreator
	public static TradeStyle getForValue(String val) {
		if (val == null) {
			throw new RuntimeException("Type is required.");
		}
		return Stream.of(TradeStyle.values())
				.filter(ele -> ele.name().equals(val))
				//.findFirst().orElseThrow(() -> new RuntimeException("Type: [" + val + "] not supported."));
				.findFirst().orElse(NOT_SUPPORTED);
	}
}
