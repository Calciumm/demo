package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.Arrays;
import java.util.stream.Stream;

public enum TradeType {
	SPOT("Spot"),
	FORWARD("Forward"),
	OPTION("VanillaOption"),
	NOT_SUPPORTED(null);

	String[] val;

	TradeType(String... val) {
		this.val = val;
	}

	@JsonCreator
	public static TradeType getForValue(String val) {
		if (val == null) {
			throw new RuntimeException("Type is required.");
		}
		return Stream.of(TradeType.values())
				.filter(ele -> Arrays.asList(ele.val).contains(val))
				//.findFirst().orElseThrow(() -> new RuntimeException("Type: [" + val + "] not supported."));
				.findFirst().orElse(NOT_SUPPORTED);
	}
}
