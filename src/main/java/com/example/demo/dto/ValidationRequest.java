package com.example.demo.dto;

import java.io.Serializable;
import java.util.Collection;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;

@Getter
@Setter
@Builder
public class ValidationRequest implements Serializable {
	private static final long serialVersionUID = -1674018769784938809L;

	Collection<TradeInformation> tradeInformations;

	@Tolerate
	public ValidationRequest() {
	}

}
